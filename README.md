# IRODS Rules 

This document will contain a list of rules that could be useful to develop for UNLOCK

-	The technician group needs to be able to “own” everything upon creation
-	Removal of hidden files and folders
-	Inherit enabled on type folders of investigation
-	Daily’ish rule to clean out the trash
-   home folder inherit recursively and own rights to the owner of the home folder
-   PID's

**Archiving Rules**

-   Archiving based on metadata value
-   Archiving based on age/last accessed (eg. only on files > 1GB) ?


**Exporting data rules**

Bulk download/export for studies or part of a study for users.

 - A tar file will be created with the requested data. This will be done interactively on data.m-unlock.nl platform. (to be implemented)
 - Make this tar file available for download only for a max amount of time (to be decided). After which it will be deleted from iRODS.

Possible solutions:
 - Explore ibun.
 - Create either a metadata field of the lifetime in hours, and calculate its doom date in the rule.
 - Create a metadata field with it's doom date upon creation of the tar.

