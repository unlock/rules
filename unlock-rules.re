#This file is available for WUR techs to write their own rules

################################################################################
# Rule to delete hidden files
################################################################################
deleteHiddenFiles{
    if (*path == "") {*abspath = "/$rodsZoneClient/home/$userNameClient"}
    else {*abspath = "/$rodsZoneClient/home/$userNameClient/*path"}
    msiGetObjType(*abspath, *type);
    if(*type == "-d") { writeLine("stdout", "ERROR: collection path expected");}
    else{
        writeLine("stdout", "Removing hidden files in *abspath");
        foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*abspath%" and DATA_NAME like "._%"){
            *coll = *row.COLL_NAME;
            *obj = *row.DATA_NAME;
            *objpath = *coll++"/"++*obj;
            
            writeLine("stdout", "DELETE: *objpath");
            msiDataObjUnlink("*objpath", *status);
            #writeLine("stdout", "DELETE: *status");
        }
        #remove orphaned metadata
        msiDeleteUnusedAVUs()
    }
}

INPUT *path=""
OUTPUT ruleExecOut

################################################################################
